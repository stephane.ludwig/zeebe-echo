FROM python:3.10

ARG GIT_HASH
ENV GIT_HASH=${GIT_HASH}

ENV PYTHONDONTWRITEBYTECODE 1
ENV PYTHONUNBUFFERED 1

WORKDIR /app

COPY requirements.txt /app/
RUN pip install -r requirements.txt

COPY zeebe_echo.py /app/

CMD ["python", "zeebe_echo.py"]